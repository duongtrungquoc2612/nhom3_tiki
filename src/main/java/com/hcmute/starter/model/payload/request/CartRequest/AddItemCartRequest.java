package com.hcmute.starter.model.payload.request.CartRequest;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Data
public class AddItemCartRequest {
    @NotEmpty(message = "Id sản phẩm không được để trống")
    String productId;
    @NotNull(message = "Số lượng sản phẩm không được trống")
    @Min(value = 1, message = "Số lượng sản phẩm phải lớn hơn 1")
    int quantity;

}

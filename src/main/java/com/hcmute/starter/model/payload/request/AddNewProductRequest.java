package com.hcmute.starter.model.payload.request;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Data
@NoArgsConstructor
@Setter
@Getter
public class AddNewProductRequest {
    @NotEmpty(message = "Tên sản phẩm không được bỏ trống")
    private String name;
    @NotEmpty(message = "Nhãn hàng không được bỏ trống")
    private UUID brand;
    @NotEmpty(message = "Lọai sản phẩm không được bỏ trống")
    private UUID category;
    @NotEmpty(message = "Số điện thoại không được để trống")
    private String description;
    @NotEmpty(message = "Giá trị sản phẩm không được để trống")
    private Double price;
    @NotEmpty(message = "Số lượng sản phẩm không đựơc bỏ trống")
    private Integer inventory;
}

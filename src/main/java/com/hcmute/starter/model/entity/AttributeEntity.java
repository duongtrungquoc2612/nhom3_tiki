package com.hcmute.starter.model.entity;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

@RestResource(exported = false)
@Entity
@Table(name = "\"attribute\"")
public class AttributeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"id\"")
    private int id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "\"category_id\"")
    private CategoryEntity attributeCategory;
    @Column(name = "\"name\"")
    private String name;
    @Column(name = "\"value\"")
    private int value;
    @Column(name = "\"suffix\"")
    private String suffix;

    public AttributeEntity() {
    }

    public AttributeEntity( CategoryEntity attributeCategory, String name, int value, String suffix) {
        this.attributeCategory = attributeCategory;
        this.name = name;
        this.value = value;
        this.suffix = suffix;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CategoryEntity getAttributeCategory() {
        return attributeCategory;
    }

    public void setAttributeCategory(CategoryEntity attributeCategory) {
        this.attributeCategory = attributeCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}

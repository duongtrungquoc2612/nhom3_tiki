package com.hcmute.starter.service;

import com.hcmute.starter.model.entity.AttributeEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
@Service
public interface AttributeService {
    List<AttributeEntity> findAll();

    AttributeEntity findById(Integer id);

    AttributeEntity saveAttribute(AttributeEntity attributeEntity);

    void deleteAttribute(Integer id);
}

package com.hcmute.starter.service;

import com.hcmute.starter.model.entity.BrandEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Component
@Service
public interface BrandService {
    BrandEntity findById(UUID id);

    BrandEntity saveBrand(BrandEntity brand);

    List<BrandEntity> findAll();

    void deleteBrand(UUID id);
}

package com.hcmute.starter.service.Impl;

import com.hcmute.starter.model.entity.AttributeEntity;
import com.hcmute.starter.model.entity.ImageProductEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.repository.AttributeRepository;
import com.hcmute.starter.repository.ImageProductRepository;
import com.hcmute.starter.repository.ProductRepository;
import com.hcmute.starter.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final AttributeRepository attributeRepository;
    private final ImageProductRepository imageProductRepository;
    @Override
    public List<ProductEntity> findAllProduct(){
        List<ProductEntity> productEntityList = productRepository.findAll();
        return productEntityList;
    }
    @Override
    public ProductEntity saveProduct(ProductEntity product) {
        return productRepository.save(product);
    }

    @Override
    public ProductEntity findById(UUID id){
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if(productEntity.isEmpty()){
            return null;
        }
        return productEntity.get();
    }
    @Override
    public void deleteProduct(UUID id){
        productRepository.deleteById(id);
    }
    @Override
    public void saveListImageProduct(List<String> listUrl, ProductEntity product){
        for (String url : listUrl){
            ImageProductEntity imageProductEntity = new ImageProductEntity();
            imageProductEntity.setUrl(url);
            imageProductEntity.setProduct(product);
            imageProductRepository.save(imageProductEntity);
        }
    }
    @Override
    public void deleteListImgProduct(ProductEntity product){
        List<ImageProductEntity> imageProductEntityList = imageProductRepository.findByProduct(product);
        for (ImageProductEntity imageProductEntity : imageProductEntityList){
            imageProductRepository.delete(imageProductEntity);
        }
    }
    @Override
    public void addAttribute(ProductEntity product, Integer attributeId){
        Optional<AttributeEntity> attribute = attributeRepository.findById(attributeId);
        product.getAttributeEntities().add(attribute.get());
        productRepository.save(product);
    }
    @Override
    public void deleteAttribute(ProductEntity product, Integer attributeId){
        Optional<AttributeEntity> attribute = attributeRepository.findById(attributeId);
        product.getAttributeEntities().remove(attribute.get());
        productRepository.save(product);
    }

}

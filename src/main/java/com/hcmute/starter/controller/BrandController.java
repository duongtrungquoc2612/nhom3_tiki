package com.hcmute.starter.controller;
import com.cloudinary.utils.ObjectUtils;
import com.hcmute.starter.mapping.BrandMapping;
import com.hcmute.starter.model.entity.BrandEntity;
import com.hcmute.starter.model.entity.CountryEntity;
import com.hcmute.starter.model.entity.userAddress.CommuneEntity;
import com.hcmute.starter.model.entity.userAddress.DistrictEntity;
import com.hcmute.starter.model.entity.userAddress.ProvinceEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.AddNewBrandRequest;
import com.hcmute.starter.service.AddressService;
import com.hcmute.starter.service.BrandService;
import com.hcmute.starter.service.ImageStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("api/brand")
@RequiredArgsConstructor
public class BrandController {
    private final BrandService brandService;
    private final ImageStorageService imageStorageService;
    private final AddressService addressService;
    @GetMapping("/all")
    public ResponseEntity<SuccessResponse> getAllBrand(){
        List<BrandEntity> brandEntityList = brandService.findAll();
        SuccessResponse response=new SuccessResponse();
        if(brandEntityList.size() == 0){
            response.setMessage("List Brand is Empty");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        response.setMessage("Query Brand Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        response.getData().put("listBrand", brandEntityList);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getBrandByID(@PathVariable UUID id){
        BrandEntity brand = brandService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(brand == null){
            response.setMessage("Brand Not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        response.setMessage("Query Brand Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        response.getData().put("brand", brand);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<SuccessResponse> updateBrand(@PathVariable UUID id,@RequestBody AddNewBrandRequest addNewBrandRequest){
        CountryEntity countryEntity = addressService.findByCountryId(addNewBrandRequest.getCountry());
        ProvinceEntity provinceEntity = addressService.findByPid(addNewBrandRequest.getProvince());
        DistrictEntity districtEntity = addressService.findByDId(addNewBrandRequest.getDistrict());
        CommuneEntity communeEntity = addressService.findByCid(addNewBrandRequest.getCommune());
        BrandEntity brand = brandService.findById(id);
        SuccessResponse response = new SuccessResponse();
        if(brand == null){
            response.setMessage("Brand Not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        brand.setBrandCountry(countryEntity);
        brand.setBrandProvince(provinceEntity);
        brand.setBrandDistrict(districtEntity);
        brand.setBrandCommune(communeEntity);
        brand.setName(addNewBrandRequest.getName());
        brand.setDescription(addNewBrandRequest.getDescription());
        brand.setAddressDetails(addNewBrandRequest.getAddressDetails());
        brand.setPhone(addNewBrandRequest.getPhone());
        brandService.saveBrand(brand);
        response.setMessage("Update Brand Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        response.getData().put("brandUpdated", brand);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping("/insert")
    public ResponseEntity<SuccessResponse> insertBrand(@RequestBody AddNewBrandRequest addNewBrandRequest){
        CountryEntity countryEntity = addressService.findByCountryId(addNewBrandRequest.getCountry());
        ProvinceEntity provinceEntity = addressService.findByPid(addNewBrandRequest.getProvince());
        DistrictEntity districtEntity = addressService.findByDId(addNewBrandRequest.getDistrict());
        CommuneEntity communeEntity = addressService.findByCid(addNewBrandRequest.getCommune());
        BrandEntity brand = BrandMapping.addBrandToEntity(addNewBrandRequest, countryEntity, communeEntity,districtEntity,provinceEntity);
        brandService.saveBrand(brand);
        SuccessResponse response=new SuccessResponse();
        response.setMessage("Insert Brand Successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping("/uploadLogo/{id}")
    public ResponseEntity<SuccessResponse> uploadLogo(@PathVariable UUID id, @RequestPart MultipartFile file) throws Exception{
        BrandEntity brand = brandService.findById(id);
        if(!imageStorageService.isImageFile(file)){
            SuccessResponse response=new SuccessResponse();
            response.setMessage("The file is not an image");
            response.setSuccess(false);
            response.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
            return new ResponseEntity<>(response, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
        String url = imageStorageService.saveLogo(file, String.valueOf(brand.getId()));
        if(url.equals("")){
            SuccessResponse response=new SuccessResponse();
            response.setMessage("Upload Logo Failure");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        brand.setImg(url);
        brandService.saveBrand(brand);
        SuccessResponse response=new SuccessResponse();
        response.setMessage("Upload Logo successfully");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<SuccessResponse> deleteBrandById(@PathVariable UUID id){
        BrandEntity brand = brandService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(brand == null){
            response.setMessage("Brand Not found");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        try {
            brandService.deleteBrand(id);
        } catch (Exception e) {
            response.setMessage("List product in Brand not Empty");
            response.setSuccess(false);
            response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        }
        response.setMessage("Brand is deleted");
        response.setSuccess(true);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

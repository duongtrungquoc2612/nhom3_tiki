package com.hcmute.starter.controller;

import com.hcmute.starter.handler.HttpMessageNotReadableException;
import com.hcmute.starter.handler.MethodArgumentNotValidException;
import com.hcmute.starter.mapping.AddressMapping;
import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.model.entity.userAddress.*;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.Address.AddNewAddressRequest;
import com.hcmute.starter.model.payload.request.Address.InfoAddressRequest;
import com.hcmute.starter.model.payload.response.ErrorResponseMap;
import com.hcmute.starter.security.JWT.JwtUtils;
import com.hcmute.starter.service.AddressService;
import com.hcmute.starter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/api/address")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;
    private final UserService userService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtils jwtUtils;
    private static final Logger LOGGER = LogManager.getLogger(AddressController.class);
    @PutMapping("")
    @ResponseBody
    public ResponseEntity<SuccessResponse> saveAddress(@RequestBody @Valid AddNewAddressRequest request, BindingResult errors, HttpServletRequest httpServletRequest) throws Exception
    {
        if (errors.hasErrors())
        {
            throw new MethodArgumentNotValidException(errors);
        }
        String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());

            if (jwtUtils.validateExpiredToken(accessToken) == true) {
                throw new BadCredentialsException("access token is  expired");
            }
            if (request == null) {
                LOGGER.info("Inside addIssuer, adding: " + request.toString());
                throw new HttpMessageNotReadableException("Missing field");
            }

            try {
                UserEntity user = userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
                AddressEntity address = AddressMapping.ModelToEntity(request);
                address.setUser(user);
                ProvinceEntity provinceEntity = addressService.findByPid(request.getProvince());
                DistrictEntity districtEntity = addressService.findByDId(request.getDistrict());
                CommuneEntity communeEntity = addressService.findByCid(request.getCommune());
                if (provinceEntity==null || districtEntity == null || communeEntity==null)
                {
                    return SendErrorValid("addresspart","Missing Address-part","Missing address-part");
                }
                else if (districtEntity.getProvince().equals(provinceEntity.getId()) && communeEntity.getDistrict().equals(districtEntity.getId())) {

                    address.setCommune(communeEntity);
                    address.setProvince(provinceEntity);
                    address.setDistrict(districtEntity);
                    AddressTypeEntity addressType = addressService.findByTid(request.getAddressType());
                    if(addressType==null)
                    {
                        SendErrorValid("AddressType","Loại địa chỉ không phù hợp","Địa chỉ sai");
                    }
                    address.setAddressDetail(request.getAddressDetail());
                    address.setAddressType(addressType);
                    addressService.saveAddress(address);
                    SuccessResponse response = new SuccessResponse();
                    response.setStatus(HttpStatus.OK.value());
                    response.setSuccess(true);
                    response.setMessage("Add address success");
                    response.getData().put("name", address.getFullName());
                    return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
                }
                else
                {
                    return SendErrorValid("AddressPart","Thông tin địa chỉ không trùng khớp","Thông tin sai");
                }
            } catch (Exception e) {
                throw new Exception(e.getMessage() + "\nAdd address fail");
            }
        }
        else throw new BadCredentialsException("access token is missing");

    }
    @GetMapping("")
    @ResponseBody
    public ResponseEntity<SuccessResponse> getUserAddress(HttpServletRequest httpServletRequest) throws Exception{
        String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if (jwtUtils.validateExpiredToken(accessToken) == true) {
                throw new BadCredentialsException("access token is  expired");
            }
            try {
                UserEntity user = userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
                if (user == null) {
                    return SendErrorValid("User", "Người dùng không hợp lệ", "Lỗi!!!!!");
                }
                List<AddressEntity> list = user.getAddress();
                if (list.isEmpty()) {
                    return SendErrorValid("Address", "Người dùng chưa có địa chỉ", "Địa chỉ trống");
                }
                SuccessResponse response = new SuccessResponse();
                response.setStatus(HttpStatus.OK.value());
                response.setMessage("Thông tin địa chỉ");
                response.setSuccess(true);
                response.getData().put("addressList", list);
                return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
            }
            catch (Exception e)
            {
                throw new Exception(e.getMessage() + "\n Get Address Fail");
            }
        }

        else
        {
            throw new BadCredentialsException("access token is missing!!!");
        }

    }
    @PostMapping("/{id}")
    @ResponseBody
    public ResponseEntity<SuccessResponse> updateUserAddress(@RequestBody @Valid InfoAddressRequest request,@PathVariable("id") String id,BindingResult errors) throws Exception
    {

            if (request == null) {
                LOGGER.info("Inside addIssuer, adding: " + request.toString());
                throw new HttpMessageNotReadableException("Missing field");
            }

            try {
                AddressEntity address = addressService.findById(id);
                ProvinceEntity provinceEntity = addressService.findByPid(request.getProvince());
                DistrictEntity districtEntity = addressService.findByDId(request.getDistrict());
                CommuneEntity communeEntity = addressService.findByCid(request.getCommune());
                if (provinceEntity==null || districtEntity == null || communeEntity==null)
                {
                    return SendErrorValid("addresspart","Missing Address-part","Missing address-part");
                }
                else if (districtEntity.getProvince().equals(provinceEntity.getId()) && communeEntity.getDistrict().equals(districtEntity.getId())) {

                    address.setCommune(communeEntity);
                    address.setProvince(provinceEntity);
                    address.setDistrict(districtEntity);
                    AddressTypeEntity addressType = addressService.findByTid(request.getAddressType());
                    if(addressType==null)
                    {
                        SendErrorValid("AddressType","Loại địa chỉ không phù hợp","Địa chỉ sai");
                    }
                    address.setAddressType(addressType);
                    address=addressService.updateAddress(address,request);
                    SuccessResponse response = new SuccessResponse();
                    response.setStatus(HttpStatus.OK.value());
                    response.setSuccess(true);
                    response.setMessage("Update address success");
                    response.getData().put("name", address.getFullName());
                    return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
                }
                else
                {
                    return SendErrorValid("AddressPart","Thông tin địa chỉ không trùng khớp","Thông tin sai");
                }
            } catch (Exception e) {
                throw new Exception(e.getMessage() + "\nAdd address fail");
            }
    }
    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<SuccessResponse> deleteAddress(@PathVariable("id") String id) throws Exception
    {
        try {
            addressService.deleteAddress(id);
            SuccessResponse response = new SuccessResponse();
            response.setStatus(HttpStatus.OK.value());
            response.setSuccess(true);
            response.setMessage("Delete address success");
            return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage() + "\nDelete address fail");
        }
    }
    @GetMapping("/province")
    @ResponseBody
    public ResponseEntity<SuccessResponse> getAllProvince() throws Exception {

        try
        {
            List<ProvinceEntity> list = addressService.getAllProvince();
            SuccessResponse response = new SuccessResponse();
            response.setStatus(HttpStatus.OK.value());
            response.setSuccess(true);
            response.setMessage("Get success");
            response.getData().put("list",list);
            return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage() +"\n get failed");
        }
    }
    @GetMapping("/district/{id}")
    @ResponseBody
    public ResponseEntity<SuccessResponse> getDistrictInProvince(@PathVariable("id") String id) throws Exception
    {
        try
        {
            List<DistrictEntity> list = addressService.getAllDistrictInProvince(id);
            SuccessResponse response = new SuccessResponse();
            if (list==null)
            {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                response.setSuccess(false);
                response.setMessage("Can't found District with id=" + id);
                return new ResponseEntity<SuccessResponse>(response,HttpStatus.NOT_FOUND);
            }

            response.setStatus(HttpStatus.OK.value());
            response.setSuccess(true);
            response.setMessage("Get success");
            response.getData().put("provinceId",id);
            response.getData().put("list",list);
            return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage() +"\n get failed");
        }
    }
    @GetMapping("/commune/{id}")
    @ResponseBody
    public ResponseEntity<SuccessResponse> getCommuneInDistrict(@PathVariable("id") String id) throws Exception
    {
        try
        {
            List<CommuneEntity> list = addressService.getAllCommuneInDistrict(id);
            SuccessResponse response = new SuccessResponse();
            if (list==null)
            {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                response.setSuccess(false);
                response.setMessage("Can't found Commune with id=" + id);
                return new ResponseEntity<SuccessResponse>(response,HttpStatus.NOT_FOUND);
            }
            response.setStatus(HttpStatus.OK.value());
            response.setSuccess(true);
            response.setMessage("Get success");
            response.getData().put("communeId",id);
            response.getData().put("list",list);
            return new ResponseEntity<SuccessResponse>(response, HttpStatus.OK);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage() +"\n get failed");
        }
    }

    private ResponseEntity SendErrorValid(String field, String message,String title){
        ErrorResponseMap errorResponseMap = new ErrorResponseMap();
        Map<String,String> temp =new HashMap<>();
        errorResponseMap.setMessage(title);
        temp.put(field,message);
        errorResponseMap.setStatus(HttpStatus.BAD_REQUEST.value());
        errorResponseMap.setDetails(temp);
        return ResponseEntity
                .badRequest()
                .body(errorResponseMap);
    }
}

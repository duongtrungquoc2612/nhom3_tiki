package com.hcmute.starter.controller;

import com.hcmute.starter.mapping.ProductMapping;
import com.hcmute.starter.model.entity.BrandEntity;
import com.hcmute.starter.model.entity.CategoryEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.AddNewProductRequest;
import com.hcmute.starter.service.BrandService;
import com.hcmute.starter.service.CategoryService;
import com.hcmute.starter.service.ImageStorageService;
import com.hcmute.starter.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final BrandService brandService;
    private final CategoryService categoryService;
    private final ImageStorageService imageStorageService;

    @GetMapping("/all")
    private ResponseEntity<SuccessResponse> showAllProduct(){
        List<ProductEntity> listProduct = productService.findAllProduct();
        if(listProduct.size() == 0) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("List Product is Empty");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Query Successfully");
        response.setSuccess(true);
        response.getData().put("List Product",listProduct);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    private ResponseEntity<SuccessResponse> showProductById(@PathVariable UUID id){
        ProductEntity product = productService.findById(id);
        if(product == null) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("Product is Not Found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Query Successfully");
        response.setSuccess(true);
        response.getData().put("Product",product);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/insert")
    private ResponseEntity<SuccessResponse> insertProduct(@RequestBody AddNewProductRequest addNewProductRequest, @RequestBody(required = false) List<Integer> listAttribute){
        BrandEntity brand = brandService.findById(addNewProductRequest.getBrand());
        CategoryEntity category = categoryService.findById(addNewProductRequest.getCategory());
        ProductEntity product = ProductMapping.addProductToEntity(addNewProductRequest,category,brand);
        productService.saveProduct(product);
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Add Product Successfully");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<SuccessResponse> updateProduct(@PathVariable UUID id,@RequestBody AddNewProductRequest addNewProductRequest){
        BrandEntity brand = brandService.findById(addNewProductRequest.getBrand());
        CategoryEntity category = categoryService.findById(addNewProductRequest.getCategory());
        ProductEntity product = productService.findById(id);
        SuccessResponse response = new SuccessResponse();
        if(product == null){
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setMessage("Product is Not Found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        product.setProductBrand(brand);
        product.setProductCategory(category);
        product.setDescription(addNewProductRequest.getDescription());
        product.setName(addNewProductRequest.getName());
        product.setPrice(addNewProductRequest.getPrice());
        product.setInventory(addNewProductRequest.getInventory());
        productService.saveProduct(product);
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Update Product Successfully");
        response.setSuccess(true);
        response.getData().put("Product",product);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<SuccessResponse> deleteProductById(@PathVariable UUID id){
        ProductEntity product = productService.findById(id);
        if(product == null) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("Product is Not Found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        productService.deleteProduct(id);
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Product is deleted");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping("/addAttribute/{id}")
    public ResponseEntity<SuccessResponse> addAttribute(@PathVariable UUID id,@RequestBody List<Integer> listAttribute){
        ProductEntity product = productService.findById(id);
        if(product == null) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("Product is Not Found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        for ( Integer i : listAttribute){
            productService.addAttribute(product,i);
        }
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Product is deleted");
        response.getData().put("AA",listAttribute);
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @DeleteMapping("/deleteAttribute/{id}")
    public ResponseEntity<SuccessResponse> deleteAttribute(@PathVariable UUID id,@RequestBody List<Integer> listAttribute){
        ProductEntity product = productService.findById(id);
        if(product == null) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("Product is Not Found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        for ( Integer i : listAttribute){
            productService.deleteAttribute(product,i);
        }
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Product is deleted");
        response.getData().put("AA",listAttribute);
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/upload/{id}",consumes = {"multipart/form-data"})
    public ResponseEntity<SuccessResponse> uploadImgInProduct(@RequestPart(required = true) MultipartFile[] multipleFiles, @PathVariable UUID id){
        ProductEntity product = productService.findById(id);
        if (product == null){
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setMessage("Product is not found");
            response.setSuccess(true);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        List<String> urls = new ArrayList<>();
        Integer index = 0;
        for (MultipartFile file : multipleFiles){
            if(!imageStorageService.isImageFile(file)){
                SuccessResponse response=new SuccessResponse();
                response.setMessage("The file is not an image");
                response.setSuccess(false);
                response.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
                return new ResponseEntity<>(response, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
            }
        }
        imageStorageService.destroyProductImg(id);
        productService.deleteListImgProduct(product);
        for (MultipartFile file : multipleFiles){
            index +=1;
            String url = imageStorageService.saveProductImg(file, id+"/"+"img"+index);
            urls.add(url);
            if(url.equals("")){
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Upload Logo Failure");
                response.setSuccess(false);
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
        }
        productService.saveListImageProduct(urls,product);
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Save image Successfully");
        response.setSuccess(true);
        response.getData().put("Info","multipleFiles");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

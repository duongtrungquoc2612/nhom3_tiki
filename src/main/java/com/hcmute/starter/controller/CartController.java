package com.hcmute.starter.controller;

import com.hcmute.starter.handler.MethodArgumentNotValidException;
import com.hcmute.starter.model.entity.CartEntity;
import com.hcmute.starter.model.entity.CartItemEntity;
import com.hcmute.starter.model.entity.ProductEntity;
import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.CartRequest.AddItemCartRequest;
import com.hcmute.starter.repository.CartItemRepository;
import com.hcmute.starter.security.JWT.JwtUtils;
import com.hcmute.starter.service.CartService;
import com.hcmute.starter.service.ProductService;
import com.hcmute.starter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.UUID;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/api/cart")
@RequiredArgsConstructor
public class CartController {
    private final CartService cartService;
    private final UserService userService;
    private final ProductService productService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtils jwtUtils;
    private static final Logger LOGGER = LogManager.getLogger(CartController.class);
    @GetMapping("")
    @ResponseBody
    public ResponseEntity<SuccessResponse> getUserCart(HttpServletRequest request) throws Error
    {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());

            if (jwtUtils.validateExpiredToken(accessToken) == true) {
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user = userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            SuccessResponse response=new SuccessResponse();
            if (user==null)
            {
                response.setMessage("Không tìm thấy user");
                response.setSuccess(false);
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
            CartEntity cart = cartService.getCartByUid(user);
            if (cart==null)
            {
                CartEntity newCart = new CartEntity((double) 0,true,user);
                cartService.saveCart(newCart);
                response.setMessage("Bạn chưa có gì trong giỏ hàng (Add cart)");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
            if (cart.getCartItem().isEmpty())
            {
                response.setMessage("Bạn chưa có gì trong giỏ hàng");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
            else {
                response.setMessage("Giỏ hàng của bạn");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                response.getData().put("Cart",cart);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        else {
            throw new BadCredentialsException("access token is missing");
        }
    }
    @PutMapping("")
    @ResponseBody
    public ResponseEntity<SuccessResponse> addItemToCart(@RequestBody @Valid AddItemCartRequest addItemCartRequest, HttpServletRequest request, BindingResult errors) throws Exception
    {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (errors.hasErrors())
        {
            throw new MethodArgumentNotValidException(errors);
        }
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if (jwtUtils.validateExpiredToken(accessToken) == true) {
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user = userService.findById(UUID.fromString(jwtUtils.getUserNameFromJwtToken(accessToken)));
            SuccessResponse response=new SuccessResponse();
            if (user==null)
            {
                response.setMessage("Không tìm thấy user");
                response.setSuccess(false);
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
            CartEntity cart = cartService.getCartByUid(user);
            ProductEntity product = productService.findById(UUID.fromString(addItemCartRequest.getProductId()));
            if (product==null)
            {
                response.setMessage("Không tìm thấy product");
                response.setSuccess(false);
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
            if(cart==null)
            {
                CartEntity newCart = new CartEntity((double) 0,true,user);
                cartService.saveCart(newCart);
            }
            CartItemEntity cartItem = cartService.getCartItemByPid(product);
            if (cartItem==null)
            {
                if (addItemCartRequest.getQuantity()<= product.getInventory()) {
                    cartItem = new CartItemEntity(cart, product, addItemCartRequest.getQuantity(), false);
                    cartService.saveCartItem(cartItem);
                    response.setSuccess(true);
                    response.setStatus(HttpStatus.OK.value());
                    response.setMessage("Thêm sản phẩm vào giỏ hàng thành công");
                    response.getData().put("Item", cartItem.getProduct().getName() + " " + cartItem.getQuantity());
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
            }
            else
            {
                if(cartItem.getQuantity() + addItemCartRequest.getQuantity()<=product.getInventory()) {
                    cartItem.setQuantity(cartItem.getQuantity() + addItemCartRequest.getQuantity());
                    cartService.saveCartItem(cartItem);
                    response.setSuccess(true);
                    response.setStatus(HttpStatus.OK.value());
                    response.getData().put("Item", cartItem.getProduct().getName() + " " + cartItem.getQuantity());
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
            }
            response.setMessage("Số lượng mua quá lớn, vui lòng kiểm tra lại");
            response.setSuccess(false);
            response.setStatus(HttpStatus.CONFLICT.value());
            return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }
        else
        {
            throw new BadCredentialsException("access token is missing");
        }
    }
}

package com.hcmute.starter.controller;

import com.hcmute.starter.model.entity.CategoryEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;
    @GetMapping("/all")
    private ResponseEntity<SuccessResponse> showAllCategory(){
        List<CategoryEntity> listCategory = categoryService.findAllCategory();
        if(listCategory.size() == 0) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("List Category is Empty");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Query Successfully");
        response.setSuccess(true);
        response.getData().put("listCategory",listCategory);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/insert")
    private ResponseEntity<SuccessResponse> insertCategory(@RequestBody CategoryEntity newCategory){
        List<CategoryEntity> foundCategory = categoryService.foundCategory(newCategory);
        if(foundCategory.size() > 0) {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.FOUND.value());
            response.setMessage("Category found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.FOUND);
        }
        categoryService.saveCategory(newCategory);
        SuccessResponse response=new SuccessResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Add Category Successfully");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    private ResponseEntity<SuccessResponse> updateCategory(@PathVariable UUID id, @RequestBody CategoryEntity newCategory, BindingResult errors) throws Exception{
        CategoryEntity category = categoryService.findById(id);
        if(category != null){
            List<CategoryEntity> foundCategory = categoryService.foundCategory(newCategory);
            if(foundCategory.size() > 0) {
                SuccessResponse response=new SuccessResponse();
                response.setStatus(HttpStatus.FOUND.value());
                response.setMessage("Category found");
                response.setSuccess(false);
                return new ResponseEntity<>(response, HttpStatus.FOUND);
            }
            category.setName(newCategory.getName());
            category.setParent(newCategory.getParent());
            category.setId(id);
            categoryService.saveCategory(category);
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.OK.value());
            response.setMessage("Update Category Successfully");
            response.getData().put("category",category);
            response.setSuccess(true);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            SuccessResponse response=new SuccessResponse();
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setMessage("Category not found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    private ResponseEntity<SuccessResponse> findCategoryById(@PathVariable UUID id){
        CategoryEntity category = categoryService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(category != null){
            response.setStatus(HttpStatus.OK.value());
            response.setMessage("Query Successfully");
            response.setSuccess(true);
            response.getData().put("category",category);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setMessage("Category not found");
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteCategory(@PathVariable UUID id){
        CategoryEntity category = categoryService.findById(id);
        SuccessResponse response=new SuccessResponse();
        if(category != null){
            try {
                categoryService.deleteCategory(id);
            } catch (Exception e) {
                response.setMessage("List brand or product in Category not Empty");
                response.setSuccess(false);
                response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
                return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
            }
            response.setStatus(HttpStatus.OK.value());
            response.setMessage("Delete Category Successfully");
            response.setSuccess(true);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setMessage("Delete Category Failure");
            response.setSuccess(true);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }
}

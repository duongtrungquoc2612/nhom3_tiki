package com.hcmute.starter.mapping;

import com.hcmute.starter.model.entity.BrandEntity;
import com.hcmute.starter.model.entity.CountryEntity;
import com.hcmute.starter.model.entity.userAddress.CommuneEntity;
import com.hcmute.starter.model.entity.userAddress.DistrictEntity;
import com.hcmute.starter.model.entity.userAddress.ProvinceEntity;
import com.hcmute.starter.model.payload.request.AddNewBrandRequest;

public class BrandMapping {
    public static BrandEntity addBrandToEntity(AddNewBrandRequest addNewBrand,CountryEntity country,CommuneEntity commune, DistrictEntity district, ProvinceEntity province ) {
        return new BrandEntity(addNewBrand.getName(),addNewBrand.getDescription(),country,addNewBrand.getPhone(),commune,district,province,addNewBrand.getAddressDetails());
    }
}

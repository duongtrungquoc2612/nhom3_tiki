package com.hcmute.starter.mapping;

import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.model.entity.userAddress.AddressEntity;
import com.hcmute.starter.model.payload.request.Address.AddNewAddressRequest;
import com.hcmute.starter.model.payload.request.Address.InfoAddressRequest;
import com.hcmute.starter.service.AddressService;

public class AddressMapping {

    public static AddressEntity ModelToEntity(AddNewAddressRequest addNewAddressRequest)
    {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setFullName(addNewAddressRequest.getFullName());
        addressEntity.setCompanyName(addNewAddressRequest.getCompanyName());
        addressEntity.setPhoneNumber(addNewAddressRequest.getPhone());
        addressEntity.setAddressDetail(addNewAddressRequest.getAddressDetail());
        return addressEntity;
    }
    public static AddressEntity UpdateAddressEntity(AddressEntity address, InfoAddressRequest addressInfo)
    {
        address.setFullName(addressInfo.getFullName());
        address.setPhoneNumber(addressInfo.getPhone());
        address.setAddressDetail(addressInfo.getAddressDetail());
        address.setCompanyName(addressInfo.getCompanyName());
        return address;
    }
}

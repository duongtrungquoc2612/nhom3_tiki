package com.hcmute.starter.repository;

import com.hcmute.starter.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.UUID;
@EnableJpaRepositories
public interface ProductRepository extends JpaRepository<ProductEntity, UUID> {

}

package com.hcmute.starter.repository;

import com.hcmute.starter.model.entity.AttributeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface AttributeRepository extends JpaRepository<AttributeEntity, Integer> {
}
